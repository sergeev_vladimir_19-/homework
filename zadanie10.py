def tuple_sort(kort):
    return tuple(sorted(kort))


kort1 = (1, 5, 2, 4, 73, 6, 3, 2)

if __name__ == "__main__":
    print(tuple_sort(kort1))
