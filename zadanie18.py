class Math:
    def __init__(self, chislo1, chislo2):
        self.chislo1 = chislo1
        self.chislo2 = chislo2

    def sum_chisel(self, chislo1, chislo2):
        return f"Сумма чисел:={self.chislo1+self.chislo2}"

    def product_chisel(self, chislo1, chislo2):
        return f"Произведение чисел:={self.chislo1*self.chislo2}"


class Dopmath(Math):
    def __init__(self, chislo1, chislo2, chislo3):
        super().__init__(chislo1, chislo2)
        self.chislo3 = chislo3

    def sredn_arifm(self, chislo1, chislo2, chislo3):
        return f"Среднее арифметическое чисел:={((self.chislo1+self.chislo2+self.chislo3)/3)}"


a = int(input('Введите первое число:='))
b = int(input('Введите второе число:='))
c = int(input('Введите третье число:='))
mathematic = Math(a, b)
mathematic1 = Dopmath(a, b, c)
print(mathematic.sum_chisel(a, b))
print(mathematic1.sredn_arifm(a, b, c))
