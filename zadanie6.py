def dicts_join(dict3, dict4):
    dict3.update(dict4)
    return dict3


dict1 = {
    'one': 500,
    'two': 4,
    'three': 400
}
dict2 = {
    'four': 600,
    'five': 5,
    'six': 8
}
if __name__ == "__main__":
    print(dicts_join(dict1, dict2))
