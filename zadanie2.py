from datetime import datetime


def definition_time(func):
    def wrapper():
        start = datetime.now()
        result = func()
        print(datetime.now()-start)
        return result
    return wrapper


@definition_time
def f():
    s = 0
    for i in range(1, 10000000):
        s += i
    return s


if __name__ == "__main__":
    print(f())
    print(f())
