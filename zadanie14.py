def list_to_set(lst):
    return set(lst)


lst1 = [1, 2, 3, 4, 5, 6, 5, 5, 6, 6, 3, 2, 7, 8, 9, 4, 'h', 'k', 'j', 'k', 'c']

if __name__ == "__main__":
    print(list_to_set(lst1))
