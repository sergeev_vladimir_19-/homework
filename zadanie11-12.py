def check_year(year):
    if year % 4 != 0:
        return False
    if year % 100 == 0:
        if year % 400 == 0:
            return True
        else:
            return False
    else:
        return False


def date(day, month, year):
    if year > 0 and (1 < month < 12):
        if month == 2 and check_year(year) is True:
            count_days_month[2] = 29
        if day in range(count_days_month[month]+1):
            return True
        else:
            return False
    else:
        return False


count_days_month = {
    1: 31,
    2: 28,
    3: 31,
    4: 30,
    5: 31,
    6: 30,
    7: 31,
    8: 31,
    9: 30,
    10: 31,
    11: 30,
    12: 31
}
days = int(input('Введите день:='))
months = int(input('Введите месяц:='))
years = int(input('Введите год:='))
if __name__ == "__main__":
    check_year(years)
    print(date(days, months, years))
