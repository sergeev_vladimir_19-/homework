def reverse_my_list(lst):
    lst[0], lst[-1] = lst[-1], lst[0]
    return lst


lst1 = ['e', 'q', 'b', 1, 2, 3, 4, 5, 6, 7, 8, 9, 'g', 'r']
if __name__ == "__main__":
    print(reverse_my_list(lst1))
